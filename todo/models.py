from django.db import models

from django.conf import settings


# Create your models here.
class TodoList(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name='todolists',
        help_text='Owner of the To-do list.',
    )

    name = models.CharField(
        max_length=300,
        help_text='Name of the To-do list.',
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        help_text='Date when this list was created.',
    )


class TodoListItem(models.Model):
    todo_list = models.ForeignKey(
        TodoList, on_delete=models.CASCADE, related_name='items',
        help_text='To-do list where this item is.',
    )

    description = models.CharField(
        max_length=2500,
        help_text='Description of the task that must be performed.',
    )

    is_done = models.BooleanField(
        help_text='Whether or not this item was already done.',
        default=False,
    )

    is_hidden = models.BooleanField(
        help_text='Whether this item should be hidden or not.',
        default=False,
    )

    done_at = models.DateTimeField(
        null=True,
        help_text='Time when the task was marked as done.',
    )

    modified_at = models.DateTimeField(
        auto_now=True,
        help_text='Last time information about this item changed.',
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
        help_text='When was this item added to the list.',
    )
