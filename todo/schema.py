"Defines types for the GraphQL API"
import web_pdb as pdb

from django.utils import timezone

from graphql.error import GraphQLError
import graphene
from graphene import relay
from graphene_django import DjangoObjectType, DjangoConnectionField

import django_filters as filters

from .models import TodoList, TodoListItem


class TodoListNode(DjangoObjectType):
    """A To-do list"""
    class Meta:
        model = TodoList
        fields = ('name', 'created_at', 'items',)
        interfaces = (relay.Node,)


class TodoListItemFilterset(filters.FilterSet):
    order_by = filters.OrderingFilter(
        fields=('done_at', 'modified_at', 'created_at',)
    )

    class Meta:
        model = TodoListItem
        fields = ('is_done', 'is_hidden',)


class TodoListItemNode(DjangoObjectType):
    """An item in the To-do list"""
    class Meta:
        model = TodoListItem
        fields = (
            'todo_list', 'description', 'is_done', 'is_hidden',
            'done_at', 'modified_at', 'created_at',
        )
        interfaces = (relay.Node,)

        filterset_class = TodoListItemFilterset


class Query(graphene.ObjectType):
    all_todo_lists = DjangoConnectionField(
        TodoListNode,
        required=True,
        description=('Get all todo lists associated with the currently '
                     'connected user.'),
    )

    todo_list = relay.Node.Field(
        TodoListNode,
        required=True,
        description="Get a todo list by it's ID.",
    )

    @staticmethod
    def resolve_all_todo_lists(root, info):
        user = info.context.user

        if user.is_anonymous:
            raise GraphQLError('You have to log-in to do that.')

        return TodoList.objects.filter(user=user)


def get_user(info):
    user = info.context.user

    if user.is_anonymous:
        raise GraphQLError('You must be logged in to perform this '
                           'operation')

    return user


# Mutations
class CreateListMutation(relay.ClientIDMutation):
    class Input:
        name = graphene.String(required=True)

    todo_list = graphene.Field(TodoListNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, name):
        user = get_user(info)

        todo_list = TodoList.objects.create(
            user=user,
            name=name
        )

        return CreateListMutation(todo_list=todo_list)


class RenameListMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)
        name = graphene.String(required=True)

    todo_list = graphene.Field(TodoListNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id, name):
        user = get_user(info)

        todo_list = relay.Node.get_node_from_global_id(info, id, TodoListNode)

        if todo_list.user != user:
            raise GraphQLError("You are not allowed to change this list")

        todo_list.name = name
        todo_list.save()
        return RenameListMutation(todo_list=todo_list)


class DeleteListMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)

    id = graphene.ID(required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id):
        user = get_user(info)

        todo_list = relay.Node.get_node_from_global_id(info, id, TodoListNode)

        if todo_list.user != user:
            raise GraphQLError("You're not allowed to change this list")

        todo_list.delete()
        return DeleteListMutation(id=id)


class AddListItemMutation(relay.ClientIDMutation):
    class Input:
        todo_list_id = graphene.ID(required=True)
        description = graphene.String(required=True)

    item = graphene.Field(TodoListItemNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, todo_list_id, description):
        user = get_user(info)
        todo_list = relay.Node.get_node_from_global_id(info, todo_list_id,
                                                       TodoListNode)

        if todo_list.user != user:
            raise GraphQLError("You're not allowed to change this list")

        item = TodoListItem.objects.create(
            todo_list=todo_list,
            description=description
        )

        return AddListItemMutation(item=item)


class UpdateListItemMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)
        description = graphene.String(required=True)
        
    item = graphene.Field(TodoListItemNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id, description):
        user = get_user(info)
        item = relay.Node.get_node_from_global_id(info, id, TodoListItemNode)

        if item.todo_list.user != user:
            raise GraphQLError("You're not alloed to change this list")

        item.description = description
        item.save()

        return UpdateListItemMutation(item=item)


class ListItemToggleDoneMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)

    item = graphene.Field(TodoListItemNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id):
        user = get_user(info)
        item = relay.Node.get_node_from_global_id(info, id, TodoListItemNode)

        if item.todo_list.user != user:
            raise GraphQLError("You're not allowed to change this list")

        if item.is_done:
            item.is_done = False
            item.done_at = None
        else:
            item.is_done = True
            item.done_at = timezone.now()

        pdb.set_trace()

        item.save()
        return ListItemToggleDoneMutation(item=item)

class ListItemToggleHiddenMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)

    item = graphene.Field(TodoListItemNode, required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id):
        user = get_User(info)
        item = relay.Node.get_node_from_global_id(info, id, TodoListItemNode)

        if item.todo_list.user != user:
            raise GraphQLError("You're not allowed to change this list")

        item.is_hidden = not item.is_hidden

        return ListItemToggleHiddenMutation(item=item)


class DeleteItemMutation(relay.ClientIDMutation):
    class Input:
        id = graphene.ID(required=True)

    id = graphene.ID(required=True)

    @staticmethod
    def mutate_and_get_payload(root, info, id):
        user = get_user(info)
        item = relay.Node.get_node_from_global_id(info, id, TodoListItemNode)

        if item.todo_list.user != user:
            raise GraphQLError("You're not allowed to change this list")

        item.delete()
        return DeleteItemMutation(id=id)


class Mutation(graphene.ObjectType):
    list_create = CreateListMutation.Field()
    list_rename = RenameListMutation.Field()
    list_delete = DeleteListMutation.Field()

    list_item_add = AddListItemMutation.Field()
    list_item_update = UpdateListItemMutation.Field()
    list_item_toggle_done = ListItemToggleDoneMutation.Field()
    list_item_toggle_hidden = ListItemToggleHiddenMutation.Field()
    list_item_delete = ListItemToggleDoneMutation.Field()
