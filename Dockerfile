FROM python:3

RUN mkdir /app/
WORKDIR /app/

COPY requirements.txt /app/

RUN apt-get update \
    && apt-get install libpq-dev \
    && apt-get clean \
    && pip install --upgrade pip && pip install ipdb 'psycopg2[binary]' \
    && pip install -r requirements.txt \
    && pip install gunicorn

COPY . /app/

CMD gunicorn -b 0.0.0.0:8000 tryingoutpdb.wsgi
