from graphene import ObjectType, Schema

import todo.schema

class Query(todo.schema.Query, ObjectType):
    "Root query object"


class Mutation(todo.schema.Mutation, ObjectType):
    "Root mutation object"


schema = Schema(query=Query, mutation=Mutation)
